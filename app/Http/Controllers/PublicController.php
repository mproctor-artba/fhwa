<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Work as Works;
use App\Project as Projects;
use App\Graph as Graphs;
use App\ChartOne as ChartOne;
use App\ChartTwo as ChartTwo;
use App\Text as Text;
use App\Ticker as Ticker;
use App\State as States;
use App\StateMap as StateMaps;

class PublicController extends Controller
{
    public function index(){

        $map = [
            "mapWidth" => "100%",
            "mapHeight" => 310,
            "shadowWidth" => 2,
            "shadowOpacity" => 0.2,
            "shadowColor" => "black",
            "shadowX" => 0,
            "shadowY" => 0,
            "iPhoneLink" => true,
            "isNewWindow" => false,
            "zoomEnable" => false,
            "zoomEnableControls" => false,
            "zoomIgnoreMouseScroll" => false,
            "zoomMax" => 8,
            "zoomStep" => 0.8,
            "borderColor" => "#ffffff",
            "borderColorOver" => "#ffffff",
            "borderOpacity" => 0.5,
            "nameColor" => "#ffffff",
            "nameColorOver" => "#ffffff",
            "nameFontSize" => "10px",
            "nameFontWeight" => "bold",
            "nameStroke" => false,
            "nameStrokeColor" => "#000000",
            "nameStrokeColorOver" => "#000000",
            "nameStrokeWidth" => 1.5,
            "nameStrokeOpacity" => 0.5,
            "overDelay" => 300,
            "nameAutoSize" => false,
            "tooltipOnHighlightIn" => false,
            "freezeTooltipOnClick" => false,
            "map_data" => []
        ];

        foreach (StateMaps::where('state', '!=' , 'Total')->get() as $state) {

            $stateData = States::where('name', $state->state)->first();

            if(!is_object($stateData)){
                dd($state);
            }
            
            $map["map_data"]{"st".$state->id} = [
                "id" => "$state->id",
                "name" => $stateData->name . ", $state->type",
                "shortname" => $stateData->abbr,
                "link" => "/states/$stateData->abbr",
                "comment" => "",
                "image" => "",
                "color_map" => env("MAP_" . $state->color . "1"),
                "color_map_over" => "#9b9ada",
                "isNewWindow" => 1
            ];
        }

        $data = [
            "page" => "home",
            "states" => States::orderBy('name', 'ASC')->get(),
            "map" => json_encode($map)
        ];


        return view('welcome', $data);
    }

    public function about(){
        $data = [
            "page" => "about"
        ];

        return view('about', $data);
    }
    public function dashboard(){
        $data = [
            "page" => "dashboard"
        ];

        return view('dashboard', $data);
    }

    public function statePage($state){

        $state = States::where('abbr', $state)->first();
        $state = $state->name;

        $data = [
            "page" => "home",
            "state" => $state,
            "works" => Works::where('stname', $state)->get(),
            "projects" => Projects::where('stname', $state)->orderBy('sequence', 'ASC')->get(),
            "chartonevalues" => ChartOne::where('stname', $state)->orderBy('sequence', 'ASC')->get(),
            "charttwovalues" => ChartTwo::where('stname', $state)->get(),
            "ticker" => Ticker::where('stname', $state)->first(),
            "text" => Text::where('state', $state)->first()
        ];


        return view('states.index', $data);
    }
}
