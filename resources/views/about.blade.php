@extends('layouts.right-aside')

@section('css')
    <style type="text/css">
    @media only screen and (min-width: 501px){
        .row .vertical-align p.header-caption{
            max-width: 80%;
            margin: 30% auto 0 auto;
            display: block;
            font-size: 24px;
        }
    }
    </style>
@endsection

@section('content')
    
<div class='col-md-12 text-center'>
    <h1 class='page-title'>About the Data</h1>
</div>
<div class="col-md-12">
    <p>Data was provided by the U.S. Federal Highway Administration (FHWA) and is from the Federal Management Information System (FMIS). Data was provided on July 1, 2019. ARTBA assigned projects to a year based on the FMIS variable for approval dates for either the construction phase of work, the preliminary engineering, right of way or planning and research phase. Federal fund totals do not include advanced construction or partial conversion of advanced construction funds.</p>
    <p>Type of work was assigned by ARTBA based on over 40 different improvement types identified in FMIS. Mode was assigned based on several different factors, including the identified improvements and construction activity. Total dollar amounts for federal, state, local, private and other funds were provided in FMIS and identified by FHWA for improvement type. Federal aid system and road type are directly from the FMIS data. The system designations include the Interstate System, additional roads that are designated as part of the National Highway System (NHS), roads that are not on the NHS but still qualify for the federal aid program and projects that are not part of the federal aid system. The NHS consists of roadways important to the nation’s economy, defense, and mobility. The map of the importance of federal-aid highway funding is based on Highway Statistics data from FHWA. It is a ten-year ratio of federal-aid reimbursements to states and the overall spending on capital outlays, which includes construction, right of way purchases and design and engineering work. It is a proxy for the importance of the federal program, which is largely spent on capital outlays.</p>
</div>
<div class="col-md-12 text-center" style="padding-top:25px;">
    For more information on the benefits of federal highway investment, go to the <a href="/dashboard" target="_blank">full dashboard</a>.
</div>
@endsection

@section('right-side')
<br><br><br>
@endsection

@section('js')
<script type="text/javascript" src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script src="https://unpkg.com/floatthead"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script>
    $(document).ready(function(){
        $("#right-aside").sticky({topSpacing:0});
      });
    </script>
@endsection