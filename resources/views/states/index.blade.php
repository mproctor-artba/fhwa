@extends('layouts.right-aside')

@section('css')
    <style type="text/css">
    @media only screen and (min-width: 501px){
        .row .vertical-align p.header-caption{
            max-width: 80%;
            margin: 30% auto 0 auto;
            display: block;
            font-size: 24px;
        }
    }
    </style>
@endsection

@section('content')
    
<div class='col-md-12 text-center'>
    <h1 class='page-title'>Federal Highway Program Impact: {{ $state }}</h1>
</div>
<div class="col-md-12 text-center">
    <p class="header-caption">{!! str_replace("percent", "percent <br>", $text->fed_cap) !!}</p>
</div>
<div class="col-md-6 text-center">
    <h3>Federal Spending by Work Type</h3>
    <div class="chart-container">
        <canvas id="canvas" height="280" style="margin-top:-75px;"></canvas>
    </div>
</div>
<div class="col-md-6 text-center">
    <h3>Federal Spending by Mode</h3>
    <div class="chart-container">
        <canvas id="canvastwo" height="280" style="margin-top:-75px;"></canvas>
    </div>
</div>
<div class="col-md-12 text-left">
    <h3>Top Federal Aid Projects: 2018</h3>
    <p>
    The following {{ $state }} highway and bridge projects begun in 2018 were the state’s largest recipients of federal-aid for that year.
    </p>
</div>
<div class="col-md-12">
    <table class="table table-striped">
        <thead>
            <tr>
                <th width="50%">Project Name</th>
                <th>Location</th>
                <th class="text-center">Federal Funds <br> (millions)</th>
                <th class="text-center">Total Cost <br> (millions)</th>
            </tr>
        </thead>
        <tbody>
            @foreach($projects as $project)
                <tr>
                    <td>{{ $project->projecttitle }}</td>
                    <td>{{ $project->ctyname }}</td>
                    <td class="text-center">${{ $project->federalfunds }}</td>
                    <td class="text-center">${{ $project->totalcost }}</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="col-md-12">
    <h3>Federal Highway Investment Deliverables in 2018</h3>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>Type of Work</th>
                <th class="text-center">Number of Projects</th>
                <th class="text-center">Federal Funds <br> (millions)</th>
                <th class="text-center">Total Cost <br> (millions)</th>
                <th class="text-center">Value/Federal %</th>
            </tr>
        </thead>
        <tbody>
            @foreach($works as $work)
                <tr>
                    <td>{{ $work->work_type }}</td>
                    <td class="text-center">{{ number_format($work->count) }}</td>
                    <td class="text-center">${{ number_format($work->federalfunds) }}</td>
                    <td class="text-center">${{ number_format($work->totalcost) }}</td>
                    <td class="text-center">{{ round($work->fed_pcnt, 3) * 100 }}%</td>
                </tr>
            @endforeach
        </tbody>
    </table>
</div>

<div class="col-md-12">
    For more information on the benefits of federal highway investment, go to the <a href="https://dashboard.artba.org/app/main#/dashboards/5e97530ae153db3beca9c92c" target="_blank">full dashboard</a>.
</div>
@endsection

@section('right-side')
    <h3 class="text-center" style="margin-bottom: 0;"># of Projects</h3>
    <div class="card card-silver">
        <div class="card-header text-center">
            <h2 class="text-center card-title slim-margins"><span class="numbercounter">{{ $ticker->count }}</span> <br> <small class="text-small"><i class=""></i></small></h2>
            <p> </p>
        </div>
        <div class="card-body">
            
        </div>
    </div>
    <h3 class="text-center" style="margin-bottom: 0;">Total Federal Funding</h3>
    <div class="card card-silver">
        <div class="card-header text-center">
            <h2 class="text-center card-title slim-margins">$<span class="numbercounter">{{ $ticker->federalfunds }}</span></h2>
            <br>
            <p style="font-size: 18px;">in Millions</p>
        </div>
        <div class="card-body">
            
        </div>
    </div>
    <h3 class="text-center" style="margin-bottom: 0;">Total Cost</h3>
    <div class="card card-silver">
        <div class="card-header text-center">
            <h2 class="text-center card-title slim-margins">$<span class="numbercounter">{{ $ticker->totalcost }}</span></h2>
            <br>
            <p style="font-size: 18px;">in Millions</p>
        </div>
        <div class="card-body">
            
        </div>
    </div>
    <!-- <a href="#" class="btn btn-danger btn-flat btn-full-width">View Full Ranking</a> -->
@endsection

@section('js')
<script type="text/javascript" src="/js/Chart.bundle.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script src="https://unpkg.com/floatthead"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
<!--
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
-->
    <script src="/js/piechart-outlabels.js"></script>
    <script>
    $(document).ready(function(){
        $("#right-aside").sticky({topSpacing:0});
      });
    </script>
    <script>
        var color = Chart.helpers.color;

        @include('layouts.chart-one')


        var colorNames = Object.keys(window.chartColors);

        @include('layouts.chart-two')

        window.onload = function() {
            var ctx = document.getElementById('canvas').getContext('2d');
            ctx.height = 400;
            window.myBar = new Chart(ctx, {
                type: 'outlabeledPie',
                data: pieChartData,
                options:{
                    plugins:{
                        /*
                      datalabels:{
                          color: "#fff",
                          outsidePadding: 20,
                          textMargin: 8,
                          font: {
                            size:12,
                          },
                          formatter: (value) => {
                            return value + "%";
                          }
                      }
                      */
                      datalabels: false,
                      legend: false,
                      outlabels: {
                           text: '%l %p',
                           color: 'white',
                           stretch: 20,
                           font: {
                               resizable: true,
                               minSize: 10,
                               maxSize: 16
                           }
                        }
                    },
                    layout: {
                        padding: {
                            left: 75,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    }
                }
            });

            var ctx2 = document.getElementById('canvastwo').getContext('2d');
            ctx2.height = 400;
            window.myBar = new Chart(ctx2, {
                type: 'outlabeledPie',
                data: ChartData,
                options:{
                    plugins:{
                        /*
                      datalabels:{
                          color: "#fff",
                          datatype: "percentage",
                          font: {
                            size:12,
                          },
                          outsidePadding: 20,
                          textMargin: 8,
                          formatter: (value) => {
                            return value + "%";
                          }
                      }
                      */
                      legend: false,
                      outlabels: {
                           text: '%l %p',
                           color: 'white',
                           stretch: 20,
                           font: {
                               resizable: true,
                               minSize: 10,
                               maxSize: 16
                           }
                        }
                    },
                    layout: {
                        padding: {
                            left: 75,
                            right: 0,
                            top: 0,
                            bottom: 0
                        }
                    }
                }
            });
            
        };
            
            $(".carousel").swipe({

              swipe: function(event, direction, distance, duration, fingerCount, fingerData) {

                if (direction == 'left') $(this).carousel('next');
                if (direction == 'right') $(this).carousel('prev');

              },
              allowPageScroll:"vertical"

            });

            function numberWithCommas(x) {
                return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
            }

            

            setTimeout( function(){ 
                $(".numbercounter").each(function(){
                var numCounter = $(this).text();

                if(numCounter > 999){
                    numCounter = numberWithCommas(numCounter);
                }


                $(this).text(numCounter);
            });
              }  , 1000 );

    </script>
@endsection