@extends('layouts.right-aside')

@section('css')
    <style type="text/css">
    @media only screen and (min-width: 501px){
        .row .vertical-align p.header-caption{
            max-width: 80%;
            margin: 30% auto 0 auto;
            display: block;
            font-size: 24px;
        }
    }
    </style>
@endsection

@section('content')
    
<div class='col-md-12 text-center'>
    <h1 class='page-title'>Federal Investment Supports $66.5 Billion in Projects in FY 2018</h1>

</div>

<div class='col-md-10 col-md-offset-1 text-center'>
<div id='map-container'></div>
<link href="/css/map.css" rel="stylesheet">
<script src="/js/raphael.min.js"></script>
<script src="/js/map.js"></script>
<script>
    var map_cfg = {!! $map !!}; 
    var map = new FlaMap(map_cfg);
    map.drawOnDomReady('map-container');
</script>

    
</div>
<div>
    <div class="col-md-12 text-left">

        <p>
            In FY 2018, states used $30.8 billion in federal-aid program funds to advance $66.5 billion in projects, according to ARTBA analysis of data from the Federal Highway Administration (FHWA). This total does not include additional federal funds obligated for projects that may have been approved in prior years.</p>
        <p>
            Over the last decade, federal investment has accounted for over 50 percent of state highway and bridge capital outlays. This investment has supported the repair and reconstruction of structures on the National Highway System (NHS), which includes the Interstate Highway System and the major roads that connect U.S. airports, ports, rail and truck terminals, pipeline terminals, and intermodal facilities. </p>
        <p>
            Click on the map to see  how each state used their federal aid dollars in FY 2018. For a deep dive into the $1.5 trillion in federal aid projects since 1950, view our interactive dashboard. <a href="/about">Click here</a> to learn more about the data.
        </p>
    </div>

        <div class="col-md-4">
            <br><br><br>
            <p>Nearly half of the projects- 45 percent – were for repair or reconstruction work. An additional 22 percent of funds were used for adding capacity, such as a new lane or major widening, to an existing roadway. Just four percent of funds were invested in new roads or bridges.</p>
        </div>
        <div class="col-md-8 text-right">
            <img src="/img/Slide1.jpeg" style="width: 100%;">
        </div>
        </div>
        <div class="col-md-8 text-left">
            <img src="/img/Slide2.jpeg" style="width: 100%;">
        </div>
        <div class="col-md-4">
            <br><br><br>
             <p>Over the last decade, federal investment has accounted for over 50 percent of state highway and bridge capital outlays. This investment has supported the repair and reconstruction of structures on the National Highway System, which includes the Interstate Highway System and the major roads that connect our airports and intermodal facilities.</p>
        </div>
        <div class="col-md-12 text-center" style="padding-top:25px;">
            For more information on the benefits of federal highway investment, go to the <a href="/dashboard" target="_blank">full dashboard</a>.
        </div>
@endsection

@section('right-side')
<br>
    <p class="text-center">Shows Federal % of annual State DOT capital outlays for highway & bridge projects</p>

<table class="table nolines">
    <tbody>
        <tr>
            <td style="width:50px; background-color: {{ env('MAP_L1') }};">     </td>
            <td>28 to 49%</td>
        </tr>
        <tr>
            <td style="background-color: {{ env('MAP_M1') }};">     </td>
            <td>50 to 69%</td>
        </tr>
        <tr>
            <td style="background-color: {{ env('MAP_H1') }};">     </td>
            <td>Over 69%</td>
        </tr>
    </tbody>
</table>

<select id="statequickselect" class="form-control" style="width:100%;">
    <option value="NULL">Select State to View Report</option>
    @foreach($states as $state)
        <option value="{{ $state->abbr }}">{{ $state->name }}</option>
    @endforeach
    </select>
    
@endsection

@section('js')
<script type="text/javascript" src="https://www.chartjs.org/dist/2.7.3/Chart.bundle.js"></script>
    <script src="https://www.chartjs.org/samples/latest/utils.js"></script>
    <script src="https://unpkg.com/floatthead"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.touchswipe/1.6.4/jquery.touchSwipe.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/chart.js@2.7.3/dist/Chart.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/chartjs-plugin-datalabels@0.7.0"></script>
    <script type="text/javascript">
        if($( window ).width() > 700){
        $("#right-aside").sticky({topSpacing:0});
    }
        $("#statequickselect").on("change", function(){
            var state = $(this).val();
            window.location.href = "/states/" + state;
        });
    </script>
@endsection