<!-- NAV -->
<div id="wrapper">
<div class="overlay" style="display: none;"></div>
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="/"><img src="https://www.artba.org/wp-content/uploads/2014/04/artbalogo1.png" style="width: 160px"></a>
    </div>
    <div id="navbar" class="navbar-collapse collapse primarymenu"> 
      <div class="menu-artba-menu-container">
        <ul id="menu-artba-menu-1" class="nav navbar-nav">
          <li id="menu-item-35471" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35471 @if($page == 'home') current-menu-item @endif">
            <a href="/">Home</a>
          </li>
          <li id="menu-item-35472" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35471 @if($page == 'about') current-menu-item @endif">
            <a href="/about">About</a>
          </li>
          <li id="menu-item-35473" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35471 @if($page == 'dashboard') current-menu-item @endif">
            <a href="/dashboard">Dashboard</a>
          </li>
          <!--
          <li class="menu-item menu-item-type-post_type menu-item-object-page  @if($page == 'state-ranking') current-menu-item @endif">
          <a href="#">Full Ranking</a>
          </li>
          <li class="menu-item menu-item-type-post_type menu-item-object-page  @if($page == 'reports') current-menu-item @endif">
          <a href="#">Economic Reports</a>
          </li>
        -->
        </ul>
      </div>      
      <ul class="nav navbar-nav navbar-right hidden-sm hidden-xs hidden-md" id="menu-artba-menu-2">
        <li><button type="button" class="hamburger is-closed animated fadeInLeft" data-toggle="offcanvas">
        <span class="hamb-top"></span>
        <span class="hamb-middle"></span>
        <span class="hamb-bottom"></span>
      </button></li>
      <li>&nbsp;</li>
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</nav>
<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
   <ul class="nav sidebar-nav" id="menu-artba-menu-3">
      
       @include('layouts.shelf')
   </ul>
</nav>          
<!-- END NAV -->
<!-- SHELF -->


</div>