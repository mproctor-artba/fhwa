var pieChartData = {
	labels: ['Reconstruction \n & Repair','Added Capacity','New Construction','Planning, Design & \n Construction Engineering','Right of Way Purchases','Other'],
  datasets: [{
              label: 'Work Types',
              borderWidth: 1,
              backgroundColor: ["#0f7899","#153951","#60b7cc","#5ba793","#f16c20","#9b9ada"],//#E76F51
              borderColor: 'rgba(200, 200, 200, 0.75)',
                hoverBorderColor: 'rgba(200, 200, 200, 1)',
              data: [
                  @foreach($chartonevalues as $chartonevalue){{ round($chartonevalue->pcnt * 100, 0) }},@endforeach
              ]
            }]
};