var ChartData = {
	labels: ["Highway", "Bridge", 'Highway & Bridge'],
	datasets: [{
		label: 'Mode',
		borderWidth: 1,
		/*
		backgroundColor: ["#0f7899","#2E81AB","#E9C46A", "#e5e5e5"],
		*/
		backgroundColor: ["#0f7899","{{ env('MAP_M1') }}","{{ env('MAP_H1') }}", "#e5e5e5"],
		borderColor: 'rgba(200, 200, 200, 0.75)',
        hoverBorderColor: 'rgba(200, 200, 200, 1)',
		data: [
			@foreach($charttwovalues as $charttwovalue)@if($charttwovalue->pcnt > 0.01) {{ round($charttwovalue->pcnt * 100, 0) }}, @endif @endforeach
		]
	}]

};

