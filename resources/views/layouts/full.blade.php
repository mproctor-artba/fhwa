<!DOCTYPE HTML>
<html class="full-height">
    <head>
        <link rel="icon" href="/img/favicon.png">
        <meta http-equiv='X-UA-Compatible' content='IE=edge' />
        <meta http-equiv='Content-Type' content='text/html; charset=utf-8'/>
        <meta name='viewport' content='width=device-width, initial-scale=1' />
        <meta name="description" content="ARTBA’s 6th Annual Bridge Report is a comprehensive analysis of the condition of bridges in the United States">
        <title>ARTBA FHWA Report</title>
        <meta property="og:url" content="https://artbabridgereport.org" />
        <meta property="og:type" content="website" />
        <meta property="og:title" content="ARTBA Bridge Report" />
        <meta property="og:description" content="ARTBA’s 6th Annual Bridge Report is a comprehensive analysis of the condition of bridges in the United States" />
        <meta property="og:image" content="https://artbabridgereport.org/img/bridgereportsocialbanner.jpg" />
        <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs-3.3.7/jszip-2.5.0/pdfmake-0.1.18/dt-1.10.13/af-2.1.3/b-1.2.4/b-colvis-1.2.4/b-flash-1.2.4/b-html5-1.2.4/b-print-1.2.4/cr-1.3.2/fc-3.2.2/fh-3.1.2/kt-2.2.0/r-2.1.1/rr-1.2.0/sc-1.4.2/se-1.2.0/datatables.min.css">
        <link rel='stylesheet' id='avada-stylesheet-css'  href='/css/style.min.css?ver=5.1.6' type='text/css' media='all' />
        <link href='https://cdnjs.cloudflare.com/ajax/libs/normalize/5.0.0/normalize.min.css' rel='stylesheet'>
        <link rel='stylesheet' href='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css'>
        <link href='https://designmodo.github.io/Flat-UI/dist/css/flat-ui.min.css' rel='stylesheet'>
        <link href='https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css' rel='stylesheet'>
        <link href='/css/theme.css' rel='stylesheet'>
        <link href="/css/artba-2019.css" rel='stylesheet'>
    @yield('css')
</head>
<body class='home page-template page-template-home page-template-index page-template-homeindex-php page page-id-18227 tribe-no-js fusion-image-hovers fusion-body no-tablet-sticky-header no-mobile-sticky-header no-mobile-slidingbar no-mobile-totop mobile-logo-pos-left layout-wide-mode fusion-top-header menu-text-align-center mobile-menu-design-modern fusion-show-pagination-text'>
        @include('layouts.nav')
        <div class='full-width-container full-height'>
            @yield('content')
        </div>
    <script src='https://code.jquery.com/jquery-3.3.1.min.js' integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=' crossorigin='anonymous'></script>
    <script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>


    @yield('js')
</body>
</html>
