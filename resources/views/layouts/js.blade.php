<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-6467000-21"></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag('js', new Date());

 gtag('config', 'UA-6467000-21');
</script>
<script src='https://code.jquery.com/jquery-3.3.1.min.js' integrity='sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=' crossorigin='anonymous'></script>
<script src='https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js'></script>
<script src='/js/scripts.js'></script>
<script type="text/javascript" src="/js/jquery.sticky.js"></script>
<script type="text/javascript">
	jQuery(document).ready(function ($) {
          var trigger = $('.hamburger'),
              overlay = $('.overlay'),
             isClosed = false;
        
            trigger.click(function () {
              hamburger_cross();      
              $('#wrapper').toggleClass('toggled');
            });

            function hamburger_cross() {
              if (isClosed == true) {          
                overlay.hide();
                trigger.removeClass('is-open');
                trigger.addClass('is-closed');
                isClosed = false;
                
              } else {   
                overlay.show();
                trigger.removeClass('is-closed');
                trigger.addClass('is-open');
                isClosed = true;
              }
          }

          $(".hamburger").click(function(){
          	$('#wrapper').toggleClass('toggled');
          });
  
        $('[data-toggle="offcanvas"]').click(function () {
        //    $('#wrapper').toggleClass('toggled');
        });
        
        $('body').on("click", ".navbar-toggle", function () {
           $(".primarymenu").toggleClass('collapse');
            console.log("show" );
        });

        $.get( "https://www.artba.org/wp-content/themes/Avada/home/shelf.php?site=bridge", function( data ) {
          $( "#menu-artba-menu-3" ).html( data );
        });
    });
</script>
@yield('js')