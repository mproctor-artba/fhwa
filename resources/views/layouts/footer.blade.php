<footer class="row footer">
    <div class="col-md-12">
        <div class="col-lg-10 col-lg-offset-1" style="padding-top: 25px;">
            <div class="col-md-9 col-sm-12">
            </div>
            <!--
            <div class="col-md-3 col-sm-12 text-right">
                <a href="/news/transportation-builder/"><img src="http://artba.artba.co/wp-content/themes/Avada/home/img/tb.png" width="200px"></a>
            </div>
            -->
            <div class="col-md-8">
                <ul>
                    <li><br><h6><a href="/"><img src="https://www.artba.org/wp-content/uploads/2016/01/logo-Copy.png" width="200px"></a></h6></li>
                    <li>Source: Data was provided by the U.S. Federal Highway Administration (FHWA) and is from the Federal Management Information System (FMIS). Data was provided on July 1, 2019. ARTBA assigned projects to a year based on the FMIS variable for approval dates for either the construction phase of work, the preliminary engineering, right of way or planning and research phase. Federal fund totals do not include advanced construction or partial conversion of advanced construction funds.</li>
                    <li><a href="https://www.artba.org/privacy-policy">Privacy &amp; Cookies Policy</a></li>
                </ul>
            </div>
            <div class="col-md-4">
                <h6><strong>Connect With Us</strong></h6>
                <ul>
                    <li><a href="https://www.facebook.com/ARTBAssociation/" target="_blank" class="social"><i class="fa fa-facebook"></i></a> <a href="https://www.linkedin.com/company/110745/" class="social"><i class="fa fa-linkedin"></i></a> <a href="https://twitter.com/ARTBA" class="social"><i class="fa fa-twitter"></i></a></li>
                    <li style="margin-top: 10px;">
                    </li>
                </ul>
            </div>
            <div class="col-md-12 text-center">
                <p>Copyright © {{ Date('Y') }} American Road &amp; Transportation Builders Association</p>
            </div>
        </div>
    </div>
</footer>