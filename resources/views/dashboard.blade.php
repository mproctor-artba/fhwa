@extends('layouts.full')

@section('content')
	<iframe id="ifm" name="ifm" width="100%" height="100%" frameborder="0" src="https://dashboard.artba.org/app/main#/dashboards/5e97530ae153db3beca9c92c/?embed=true&h=false" embed="true" scrolling="auto" style="margin:0; padding:0; height: 100%; width: 100%; border:0; position: absolute; margin-top:0;"></iframe>
	<!-- Global site tag (gtag.js) - Google Analytics -->
@endsection

@section('js')
	<script src="https://dashboard.artba.org/js/frame.js"></script>
	<script type="text/javascript" src="/js/jquery-ui.min.js"></script>
@endsection
